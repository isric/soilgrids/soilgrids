#!/bin/bash
# Sets the source folder and loads all the environment variables.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 22-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

main()
{
	if [ -z "$SRCDIR" ]
	then
		echo "SRCDIR environment variable not set. This should be the path to the local SoilGrids250m repository."
		return
	fi
	
    if [ -z "$CONFDIR" ]
	then
		echo "CONFDIR environment variable not set. This should be the path with the config files for your project."
		return
	fi
	
	if [ -z "$1" ]
	then
		echo "Please provide a property name, e.g. 'soc'"
		return
	fi
	
	source $CONFDIR/config_main.sh $1
	
	
	if [ -z "$2" ]
	then
		echo "Please provide a main RUN name, e.g. 'RUN02'. This identifies the model used."
		return
	fi
	
	export RUN=$2
	
	
	if [ -z "$3" ]
	  then
		echo "Empty RUN_PRED argument. Default = main RUN. This is to identify the prediction run."
	    export RUNPRD=$RUN 
	  else
		export RUNPRD=$3    
	fi
	
	
	source $CONFDIR/config_model.sh
	source $CONFDIR/config_slurm.sh
	source $CONFDIR/config_overlay.sh
	
	if [ ! -f $PARAMDIR/config_voi_$VOIDIR.sh ]
	then
		echo "Folder creation for variable $1"
		source $SRCDIR/002_dirs_creation.sh 
		echo ""
		echo "All done."
		echo "Please edit $PARAMDIR/config_voi_$VOIDIR.sh before running again."
	else
		source $PARAMDIR/config_voi_$VOIDIR.sh
	fi
}


main $1 $2 $3

