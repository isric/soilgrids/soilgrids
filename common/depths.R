# Handy functions to create vectors with depths intervals
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 03-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

getDepthsVector <- function()
{
	return(as.numeric(unlist(strsplit(Sys.getenv("DPTINT"),","))))
}

getDepthsMidPoints <- function()
{
    if(length(getDepthsVector())>1){
        deptInts <- getDepthsVector()
        return(unlist(lapply(2:length(deptInts),
                    function(x){deptInts[x-1]+((deptInts[x]-deptInts[x-1])/2)})))	
    } else {
        return(NA)
    }
}

getDepthsFolders <- function()
{
	if(length(getDepthsVector())>1){
        deptInts <- getDepthsVector()
        return(unlist(lapply(1:(length(deptInts)-1), 
                    function(d){paste0(deptInts[d],"-",deptInts[d+1],"cm")})))	
    } else {
        return(paste0(Sys.getenv("DPTINT"),"cm"))
    }
}



