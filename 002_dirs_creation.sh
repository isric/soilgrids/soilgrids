#!/bin/bash
# Creates necessary folders for model inputs and outputs. Also places a 
# template config file for the variable of interest (VOI).
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

for dir_create in $INDIR $CVDIR $PREDIR $PARAMDIR $COVSDIR $PLOTDIR $RFEDIR; do 
    echo "Creating directory " $dir_create
    
        if [ ! -d "$dir_create" ]; then
            mkdir -p $dir_create   
            chmod -R g+rws $dir_create   
        fi #
done


# Copy template config file 
cp $CONFDIR/config_voi.sh.dist $PARAMDIR/config_voi_$VOIDIR.sh
