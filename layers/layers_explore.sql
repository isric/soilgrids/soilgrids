-- #######################################################
-- Overlapping layers in WoSIS

SELECT l.profile_id, l.upper_depth, l.lower_depth,
		ol.profile_id, ol.upper_depth, ol.lower_depth
  FROM soilgrids.wosis_201901_layers l
	INNER JOIN                                            
		   (SELECT profile_id, lower_depth, upper_depth 
		      FROM soilgrids.wosis_201901_layers) ol                            
    ON l.profile_id = ol.profile_id                        
 WHERE l.profile_id < 100000
   AND l.lower_depth > ol.upper_depth
   AND l.lower_depth < ol.lower_depth
 ORDER BY l.profile_id,l.upper_depth,ol.upper_depth ASC; 

SELECT COUNT(*)
  FROM soilgrids.wosis_201901_layers l
	INNER JOIN                                            
		   (SELECT profile_id, lower_depth, upper_depth 
		      FROM soilgrids.wosis_201901_layers) ol                            
    ON l.profile_id = ol.profile_id                        
 WHERE l.lower_depth > ol.upper_depth
   AND l.lower_depth < ol.lower_depth; 

SELECT COUNT(*)
  FROM soilgrids.wosis_201901_layers l
	INNER JOIN                                            
		   (SELECT profile_id, lower_depth, upper_depth 
		      FROM soilgrids.wosis_201901_layers) ol                            
    ON l.profile_id = ol.profile_id                        
 WHERE l.upper_depth < ol.upper_depth
   AND l.upper_depth > ol.lower_depth;   

SELECT MIN(profile_id) FROM  soilgrids.wosis_201901_layers;

-- #######################################################
-- Overlapping layers in Rapid Stream

-- Count number of layers
SELECT COUNT(*)
  FROM soilgrids.rs_layers l
	INNER JOIN                                            
		   (SELECT profile_code, lower_depth, upper_depth 
		      FROM soilgrids.rs_layers) ol                            
    ON l.profile_code = ol.profile_code                        
 WHERE l.lower_depth > ol.upper_depth
   AND l.lower_depth < ol.lower_depth; 

SELECT COUNT(*)
  FROM soilgrids.rs_layers l
	INNER JOIN                                            
		   (SELECT profile_code, lower_depth, upper_depth 
		      FROM soilgrids.rs_layers) ol                            
    ON l.profile_code = ol.profile_code                        
 WHERE l.upper_depth < ol.upper_depth
   AND l.upper_depth > ol.lower_depth; 
   
-- Count number of affected profiles   
SELECT COUNT(DISTINCT(l.profile_code))
  FROM soilgrids.rs_layers l
	INNER JOIN                                            
		   (SELECT profile_code, lower_depth, upper_depth 
		      FROM soilgrids.rs_layers) ol                            
    ON l.profile_code = ol.profile_code                        
 WHERE l.lower_depth > ol.upper_depth
   AND l.lower_depth < ol.lower_depth; 
   
-- #######################################################
-- Layers per profile

CREATE VIEW soilgrids.v_temp AS
SELECT profile_code, COUNT(*)
  FROM soilgrids.rs_layers
 GROUP BY profile_code;

SELECT * FROM soilgrids.v_temp ORDER BY count DESC LIMIT 100;

SELECT COUNT(*) FROM soilgrids.v_temp WHERE count > 10;

DROP VIEW v_temp;


   
