#!/bin/bash
# Retrieves layers collections from the GRASS database to create a union
# of all layers. Also creates synthetic layers and applies transformation.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 15-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### Union layers and join (including mid-point dpeth for each horizon)
# If no pedotransfer functions then use union_layers, otherwise apply function
if test -z "$PDTF"; then
    $R_EXEC $SRCDIR/layers/union_layers.R
else
    source $SRCDIR/layers/pdtfuns/"main_"$PDTF".sh" 
fi


# ### data transformation
if test ! -z "$TRANSFORM"; then
    $R_EXEC $SRCDIR/layers/data_transformation.R # log and ALR
fi
