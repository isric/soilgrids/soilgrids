# ### Data preparation for the function
$R_EXEC $SRCDIR/layers/pdtfuns/data_table_ocd.R

# ### Fitting and predictions of the function
# TODO: Cross-valid here?
$R_EXEC $SRCDIR/layers/pdtfuns/fit_pdtf_ocd.R

# ### Coarse fragments correction and OCD regression matrix
# # # grass76 ./GRASSDATA/global_ighomolosine/poggi002
# # # 
# # # g.copy profiles_folds, profiles_pdtf
# # # 
# # # for vrt in $OUTDIR/cfvo/ranger_RUN03/predict/RUN01/tiff/*Q0.5*vrt; do echo $vrt
# # #     map=`echo $(basename $vrt) | rev | cut -d. -f2-3 | rev`
# # #     time r.external input=$vrt output=$map -o
# # # done

export cfvo_run=RUN06
$R_EXEC $SRCDIR/layers/pdtfuns/overlay_ocd.R
