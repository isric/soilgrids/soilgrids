library(RPostgreSQL)
library(tidyverse)

source(paste0(Sys.getenv("SRCDIR"),"common/reg_matrix.R"))

connGRASS = dbConnect(PostgreSQL(), 
		host=Sys.getenv("PG_HOST"), 
		port=Sys.getenv("PG_PORT"), 
		user=Sys.getenv("PG_USER"), 
		password=Sys.getenv("PG_PASS"), 
		dbname=Sys.getenv("PG_DB_GRASS"))

connPg <-  dbConnect(PostgreSQL(), 
		host=Sys.getenv("PG_HOST"), 
		port=Sys.getenv("PG_PORT"), 
		user=Sys.getenv("PG_USER"), 
		password=Sys.getenv("PG_PASS"), 
		dbname=Sys.getenv("PG_DB"))

vois=unlist(strsplit(c("soc","phh2o","bdod","sand","silt","clay","cfvo"),","))
# vois_wise  <- unlist(strsplit(c("ORGC","PHH2O","","SAND","SILT","CLAY","GRAVEL"),","))
vois_wise  <- ""

rel_layer <- paste0(Sys.getenv("PG_SCHEMA_OBS"), ".", Sys.getenv("REL_LAYER"))
rel_profile <- paste0(Sys.getenv("PG_SCHEMA_OBS"), ".", Sys.getenv("REL_PROFILE"))

# Booleans are different between SQLite and Postgres
db_true <- Sys.getenv("DB_TRUE")

message("Reading tables from GRASS into R...")
tbl.loc=dbReadTable(connGRASS, Sys.getenv("PROFILES_COVS"), 
					schema=Sys.getenv("MAPSET_USER")) 
tbl.fld=dbReadTable(connGRASS, Sys.getenv("PROFILES_FOLD"), 
					schema=Sys.getenv("MAPSET_USER"))

message("Sending queries to database ...")

source(paste0(Sys.getenv("SRCDIR"),"layers/query_wosis.R"))
tbl.lyr=dbGetQuery(connPg, query)
message(paste0("Number of sampled layers : ", nrow(tbl.lyr)))

if(vois_wise!=""){
    source(paste0(Sys.getenv("SRCDIR"),"layers/query_wise.R"))
    tbl.wise=dbGetQuery(connGRASS, wiseQuery)
    tbl.lyr=rbind(tbl.lyr, tbl.wise)
    message(paste0("Number of synthetic layers : ", nrow(tbl.wise)))
}

# ### Joins with spatial data
tbl.prof=full_join(tbl.loc,tbl.fld, by="id_profile")
tbl=inner_join(tbl.prof,tbl.lyr, by="id_profile") 
tbl=tbl[,grep("\\.y$",names(tbl),invert=T)]
names(tbl)=gsub("\\.x$","",names(tbl))

tbl[,Sys.getenv("DEPTH_COL")]=as.numeric(tbl$bottom) + ((as.numeric(tbl$top) - as.numeric(tbl$bottom)) /2)
message(paste0("Mid-point depth of horizons calculated"))

tbl <- tbl[tbl[[Sys.getenv("DEPTH_COL")]]>0,]

message(paste0("Number of layers to be saved: ", nrow(tbl)))



saveRDS(tbl, file=paste0(Sys.getenv("MODDIR"), 
        "data_table_",gsub(",","_",Sys.getenv("VOI")),".RDS"))
        
dbDisconnect(connPg)
dbDisconnect(connGRASS)

