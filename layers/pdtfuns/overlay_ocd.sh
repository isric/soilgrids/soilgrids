cat $MODDIR"/ptf/coordinates.csv" | awk 'BEGIN { FS=OFS=","};{ print $2, $3 }' | sed -e '1d' | tr "," " " > $MODDIR"/ptf/coordinates_gdal.csv"

for vrt in $OUTDIR/cfvo/ranger_$cfvo_run/predict/RUN01/tiff/*Q0.5*vrt; do echo $vrt
    map=`echo $(basename $vrt) | rev | cut -d. -f2-3 | rev`
    time cat $MODDIR"/ptf/coordinates_gdal.csv" | gdallocationinfo -valonly -geoloc $vrt > $MODDIR"/ptf/"$map".txt"
    f=$(ls -1tr $MODDIR"/ptf/"$map".txt" | head -1); echo $map | cat - $f > tmp.txt && rm $f && mv tmp.txt $MODDIR"/ptf/"$map".txt"
done

files_sort=`ls $MODDIR/ptf/cfvo_* | sort -t _ -k 3 -g`
paste -d , $MODDIR"/ptf/coordinates.csv" $files_sort > $MODDIR"/ptf/coordinates_cfvo.csv"


