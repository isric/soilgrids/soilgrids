#!/usr/bin/python3
#
# Aggregates WRB probabilites to the RSG level.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 20-01-2020
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import sys
import os
import glob
import jsonpickle
import rasterio
import numpy as np
import configparser

sys.path.append(os.getcwd() + '/..')
from tiler import Tiler

tiler = None

def aggregate(rsg):

    paths = glob.glob(path_SG + "*" + rsg + "*.tif")
    tile_brick = np.ndarray(shape=(len(paths), tiler.window.height, tiler.window.width), dtype=float)

    for i in range(len(paths)):
        print("Reading tile for raster: " + paths[i])
        tile_brick[i] = tiler.loadTileFromWindowNum(paths[i])

    result_f = np.sum(tile_brick, axis=0)
    result_f = np.nan_to_num(result_f, nan=tiler.nodata)
    result = np.asarray(result_f, tiler.dtype)

    tiler.saveTile(result, out_folder + rsg + "/" + str(tiler.windowNum) + ".geotiff") 

    print("Saved tile: " + out_folder + rsg + "/" + str(tiler.windowNum) + ".geotiff")

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Please provide a window side and a tile number.")
        exit
		
    config = configparser.ConfigParser()
    config.read('config')
    rsg_file = config['WRB']['rsg_file']
    path_SG = config['WRB']['path_SG']
    out_folder = config['WRB']['path_RSG']
    
    f = open(rsg_file)
    json_str = f.read()
    rsg_array = jsonpickle.decode(json_str)

    # Get path to random tiff to institiate Tiler class
    paths = glob.glob(path_SG + "*.tif")
    tiler = Tiler(paths[0], int(sys.argv[1]), int(sys.argv[2]))
	
    for rsg in rsg_array:
       aggregate(rsg) 

    # TEsting
#    aggregate(rsg_array.pop())
