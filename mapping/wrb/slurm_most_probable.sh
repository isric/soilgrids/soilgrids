#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=MostProbable
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=luis.desousa@wur.nl
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=/home/WUR/duque004/temp/slurm_logs/most_probable_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment='5318018039'

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --time=4
#SBATCH --constraint 32cpus
#SBATCH --array=1-731%64
#SBATCH --mem-per-cpu=8000

#-----------------------------Environment, Operations and Job steps----

echo 'Hello '${SLURM_ARRAY_TASK_ID}

source ~/my_envs/p36_soilgrids/bin/activate

python3 -u wrb-most-probable.py 4096 ${SLURM_ARRAY_TASK_ID}

