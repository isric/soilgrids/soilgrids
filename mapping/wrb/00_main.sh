#!/bin/bash
#
# Just to explain the order in which the various programmes should be run.
# Running this script will not produce meaning results.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 13-03-2020
# 
# Copyright (c) 2020 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Identify unique RSGs mapped
python3 wrb-unique-groups.py

# Aggregate predictions at the RSG for a tile
# slurm_aggregation.sh provides execution in parallel with Slurm
python3 wrb-aggregate.py 4096 162

# Identify most probabl RSG for each cell in a tile
# slurm_most_probable.sh provides execution in parallel with Slurm
python3 wrb-most-probable.py 4096 162

# Create VRT for a RSG
# slurm_vrt provides execution in parallel with Slurm
bash create_vrt.sh

# Create VRT for most probable RSG, adding raster attribute table
python3 wrb-rat.py
