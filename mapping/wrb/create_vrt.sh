#!/bin/bash
# Creates VRT for a given folder containing contiguous GeoTIff files. In the 
# HPC cluster the GDAL library must be loaded beforehand, e.g.:
#
# module load gdal/gcc/64/2.2.2
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 31-01-2020
# 
# Copyright (c) 2020 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

if [ -z "$1" ]
then
  echo "Please provide a folder number to process"
  exit 1
fi

PATH_IN="/home/duque004/HPC/ISRIC/SG2017/WRB/"
cd $PATH_IN

folders=`ls -d */ | cut -f1 -d'/' | tr " " "\n"`
folder=`echo $folders | cut -d " " -f $1`

gdalbuildvrt $folder.vrt $folder/*.geotiff

gdaladdo -ro --config COMPRESS_OVERVIEW DEFLATE \
         --config BIGTIFF_OVERVIEW YES $folder.vrt 4 16 64 256	

#echo "Folders: "$folders

echo "Completed VRT for: "$folder

