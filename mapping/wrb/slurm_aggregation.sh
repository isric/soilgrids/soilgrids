#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=Mapping
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=luis.desousa@wur.nl
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=/home/WUR/duque004/temp/slurm_logs/mapping_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment='5318018039'

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --constraint 32cpus
#SBATCH --time=20
#SBATCH --array=1-731%100
#SBATCH --mem-per-cpu=5000

#-----------------------------Environment, Operations and Job steps----

echo 'Hello '${SLURM_ARRAY_TASK_ID}

source ~/my_envs/p36_soilgrids/bin/activate

python3 -u wrb-aggregate.py 4096 ${SLURM_ARRAY_TASK_ID}

