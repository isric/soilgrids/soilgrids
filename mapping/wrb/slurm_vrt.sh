#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=MappingVRT
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=luis.desousa@wur.nl
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=/home/WUR/duque004/temp/slurm_logs/mapping_vrt_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment='5318018039'

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --time=5
#SBATCH --array=1-32
#SBATCH --mem-per-cpu=4000

#-----------------------------Environment, Operations and Job steps----

module load gdal/gcc/64/2.2.2

bash create_vrt.sh $SLURM_ARRAY_TASK_ID


