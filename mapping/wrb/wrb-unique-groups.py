#!/usr/bin/python3
#
# Identifies the unique RSGs mapped the in 2017 release of SoilGrids.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 19-01-2020
# 
# Copyright (c) 2020 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################
import glob
import jsonpickle
import configparser

if __name__ == "__main__":
    
    config = configparser.ConfigParser()
    config.read('config')
    rsg_file = config['WRB']['rsg_file']
    path_2017 = config['WRB']['path_2017']

    rsg_array = set()
    paths = glob.glob(path_2017)
    
    for path in paths:
        full_name = path.split("TAXNWRB_")[1].split("_")[0]
        rsg_array.add(full_name.split(".")[1])

    print(jsonpickle.encode(rsg_array), file=open(rsg_file, "w"))
 
    print("The set of unique RSGs:")
    print(rsg_array)
    print("Saved result to " + rsg_file)
    

