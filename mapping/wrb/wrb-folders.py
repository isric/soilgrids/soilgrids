#!/usr/bin/python3
#
# reates output folders, one for each WRB RSG.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 27-01-2020
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import sys
import os
import jsonpickle
import configparser

if __name__ == "__main__":
		
    config = configparser.ConfigParser()
    config.read('config')
    rsg_file = config['WRB']['rsg_file']
    out_folder = config['WRB']['path_RSG']

    f = open(rsg_file)
    json_str = f.read()
    rsg_array = jsonpickle.decode(json_str)
	
    for rsg in rsg_array:
        os.makedirs(out_folder + rsg)

