#!/usr/bin/python3
#
# Determines the most probable Reference Soil Group (RSG).
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 17-02-2020
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import sys
import os
import jsonpickle
import rasterio
import numpy as np
import configparser

sys.path.append(os.getcwd() + '/..')
from tiler import Tiler

rsg_file = None
path_RSG = None
out_folder = None
tiler = None


def findMostProbable(rsg_list):

    tile_brick = np.ndarray(shape=(len(rsg_list), tiler.window.height, tiler.window.width), dtype=float)
    result_f = np.empty(shape=(tiler.window.height, tiler.window.width), dtype=float)

    for i in range(len(rsg_list)):
        print("Reading tile for raster: " + path_RSG + "/" + rsg_list[i] + ".vrt")
        tile_brick[i] = tiler.loadTileFromWindowNum(path_RSG + "/" + rsg_list[i] + ".vrt")

    for i in range(tile_brick.shape[1]):
        for j in range(tile_brick.shape[2]):
            if(np.isnan(tile_brick[:,i,j].max())):
                result_f[i,j] = np.nan
            else:
                result_f[i,j] = tile_brick[:,i,j].argmax()

    result_f = np.nan_to_num(result_f, nan=tiler.nodata)
    result = np.asarray(result_f, tiler.dtype)

    tiler.saveTile(result, out_folder + "/" + str(tiler.windowNum) + ".geotiff") 
    print("Saved tile: " + out_folder + "/" + str(tiler.windowNum) + ".geotiff")


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Please provide a window side and a tile number.")
        exit

    config = configparser.ConfigParser()
    config.read('config')
    rsg_file = config['WRB']['rsg_file']
    path_RSG = config['WRB']['path_RSG']
    out_folder = config['WRB']['out_folder_most']

    f = open(rsg_file)
    json_str = f.read()
    f.close()
    rsg_list = list(jsonpickle.decode(json_str))

    # Get path to random raster to institiate Tiler class
    tiler = Tiler(path_RSG + "/" + rsg_list[0] + ".vrt", int(sys.argv[1]), int(sys.argv[2]))

    findMostProbable(sorted(rsg_list))

