#!/usr/bin/python3
#
# Creates attribute table for the raster with the most probable Reference Soil 
# Group (RSG).
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 18-02-2020
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

from osgeo import gdal
import configparser
import jsonpickle
import subprocess

def create_vrt(out_folder):

    subprocess.call("gdalbuildvrt " + out_folder + ".vrt " + out_folder + "/*.geotiff", shell=True)

    subprocess.call("gdaladdo -ro --config COMPRESS_OVERVIEW DEFLATE " +
         "--config BIGTIFF_OVERVIEW YES " + out_folder + ".vrt 4 16 64 256", shell=True)


def addAttributeTable(rsg_list, out_folder):

    ds = gdal.Open(out_folder + ".vrt")
    rb = ds.GetRasterBand(1)

    # Create and populate the RAT
    rat = gdal.RasterAttributeTable()
    rat.CreateColumn('VALUE', gdal.GFT_Integer, gdal.GFU_Generic)
    rat.CreateColumn('RSG', gdal.GFT_String, gdal.GFU_Generic)

    for i in range(len(rsg_list)):
        print("RAT entry: " + str(i) + "-" + rsg_list[i])
        rat.SetValueAsInt(i, 0, i)
        rat.SetValueAsString(i, 1, rsg_list[i])

    # Associate with the band
    rb.SetDefaultRAT(rat)
    # Close the dataset and persist the RAT
    ds = None


if __name__ == "__main__":

    config = configparser.ConfigParser()
    config.read('config')
    rsg_file = config['WRB']['rsg_file']
    out_folder = config['WRB']['out_folder_most']

    create_vrt(out_folder)

    f = open(rsg_file)
    json_str = f.read()
    f.close()
    rsg_list = list(jsonpickle.decode(json_str))

    addAttributeTable(sorted(rsg_list), out_folder)

