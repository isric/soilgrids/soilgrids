#!/bin/bash
# Maps uncertainties in parallel according to the expression:
#
# ((q95(s)-q5(s))/median(s)) / max_over all_s{(q95(s)-q5(s))/median(s)} * 100%
#
# This is just an example, the same tilling mechanism can be use to perform 
# most other mapping operations in parallel.
#
# TODO:
# - paths are still hard-code
# - maps only one raster, must be scaled for multiple raster in the same run
# - sort out data type in output raster
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 15-11-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Compute maximum median distances per tile
sbatch slurm_medians.sh

# Compute overall maximum distance to median
python3 findMax.py ./medians

# Map out final result
sbtach slurm_mapping.sh

# Assemble VRT
gdalbuildvrt ./data/map.vrt ./data/*.getiff
gdaladdo -ro --config COMPRESS_OVERVIEW DEFLATE \
			--config BIGTIFF_OVERVIEW YES ./data/map.vrt 4 16 64 256
