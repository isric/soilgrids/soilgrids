#!/usr/bin/python3
#
# Maps uncertainty for a given tile according to the expression below:
#
# ((q95(s)-q5(s))/median(s)) / max_over all_s{(q95(s)-q5(s))/median(s)} * 100%
#
# The max component is loaded from a text file (it must computed beforehand).
#
# TODO:
# - the ComputeTile class is probably unecessary: simplify
# - retireve raster paths from environment variables
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 20-11-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import sys
import rasterio
import numpy as np
from affine import Affine

sys.path.append(os.getcwd() + '/..')
from tiler import Tiler

windowSide = 4096
windowNum = 100

class ComputeTile(Tiler):

	self.path_max = "./medians/max.txt"
    self.q95_suffix = "_Q0.95.vrt"
	self.q50_suffix = "_Q0.5.vrt"
	self.q05_suffix = "_Q0.05.vrt"
	
	def __init__(self, rast_path, windowSide, windowNum):
	
		super(ComputeTile,self).__init__(rast_path+self.q95_suffix, windowSide, windowNum)
		

	def compute(self, path):

		# Load maximum computed earlier
		f = open(self.path_max, "r")
		max = float(f.read())
		f.close();

		# Read the tiles
		w95f = self.loadTileFromWindowNum(path + self.q95_suffix)
		w50f = self.loadTileFromWindowNum(path + self.q50_suffix)
		w05f = self.loadTileFromWindowNum(path + self.q05_suffix)

		# Compute the ratio
		result = np.divide(np.subtract(w95f, w05f), w50f)
		
		# Divide by max
		result = np.divide(result, max)
		result = np.multiply(result, 100)
		
        self.saveTile(result, "./data/tile" + str(windowNum) + ".geotiff")


if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("Please provide a window side and a tile number.")
	else:
		windowSide = int(sys.argv[1])
		windowNum = int(sys.argv[2])
		#path = sys.
		path = "/home/duque004/HPC/ISRIC/SG2019/v2.0.0/outputs/nitrogen/ranger_RUN03/predict/RUN01/tiff/nitrogen_0-5cm"
		tiler = ComputeTile(path, windowSide, windowNum)
		tiler.compute(path)

