#!/usr/bin/python3
#
# Computes the expression below for a given tile:
#
# max_over_all_s{(q95(s)-q5(s))/median(s)
#
# TODO:
# - the ComputeMedians class is probably unecessary: simplify
# - retireve raster paths from environment variables
# - deal with divisions by zero
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 15-11-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import sys
import numpy as np

sys.path.append(os.getcwd() + '/..')
from tiler import Tiler

class ComputeMedians(Tiler):

	self.path_medians = "./medians/"
    self.q95_suffix = "_Q0.95.vrt"
	self.q50_suffix = "_Q0.5.vrt"
	self.q05_suffix = "_Q0.05.vrt"

	def __init__(self, rast_path, windowSide, windowNum):
	
		super(ComputeMedians,self).__init__(rast_path + self.q95_suffix, windowSide, windowNum)


	def compute(self, path):

		# Read the tiles
		w95f = self.loadTileFromWindowNum(path + self.q95_suffix)
		w50f = self.loadTileFromWindowNum(path + self.q50_suffix)
		w05f = self.loadTileFromWindowNum(path + self.q05_suffix)

		# Compute the ratio
		result = np.divide(np.subtract(w95f, w05f), w50f)

		# Save result to file
		print(str(np.nanmax(result)),  file=open(self.path_medians + str(self.windowNum) + ".txt", "w"))


if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("Please provide a window side and a tile number.")
	else:
		#path = sys.
		path = "/home/duque004/HPC/ISRIC/SG2019/v2.0.0/outputs/nitrogen/ranger_RUN03/predict/RUN01/tiff/nitrogen_0-5cm"
		medians = ComputeMedians(path, int(sys.argv[1]), int(sys.argv[2]))
		medians.compute(path)

