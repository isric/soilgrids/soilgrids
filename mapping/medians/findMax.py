#!/usr/bin/python3
#
# Completes the computation of the expression below for a whole raster set:
#
# max_over_all_s{(q95(s)-q5(s))/median(s)}
#
# Reads the individual tile medians from txt files computed beforehand.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 19-11-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################
import sys
import os
import glob

def findMax(path):

	max = sys.float_info.min
	for filename in glob.glob(os.path.join(path, '*.txt')):
	  with open(filename, 'r') as f:
	  	value = float(f.read())
	  	if (value < float("Inf")) and (value > max):
	  		max = value
	  	f.close()
    		
	print ("The maximum is: ", str(max))
	print(str(max),  file=open(path + "max.txt", "w"))
    	
if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Please provide a path to folder with tiled medians.")
	else:
		findMax(sys.argv[1])


