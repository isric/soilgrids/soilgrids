#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=Mapping
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=luis.desousa@wur.nl
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=slurm_logs/mapping_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment='5318018039'

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --time=1
#SBATCH --array=1-372
#SBATCH --mem-per-cpu=1500

#-----------------------------Environment, Operations and Job steps----

source ~/my_envs/p36_soilgrids/bin/activate

python3 computeTile.py 5120 ${SLURM_ARRAY_TASK_ID}

