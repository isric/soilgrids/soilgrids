#!/usr/bin/python3
#
# Template for an ad hoc mechanism based on given tile side. The constructors
# takes as arguments a path to a large raster, a tile side and a tile number. 
# Tiles are numbered sequentially from left to right and top to bottom. It 
# loads all metadata necessary for the desired tile.
#
# Also offers a method to load an array from a given raster corresponding to 
# the tile and location set by the constructor. 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 03-12-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:
###############################################################################

import rasterio
import numpy as np
from rasterio.windows import Window
from affine import Affine

class Tiler:

    windowSide = None
    windowNum = None
    crs = None
    dtype = None
    nodata = None
    window = None
    affine = None
	
    def __init__(self, rast_path, windowSide, windowNum):
	
		# Load meta-data
        rast = rasterio.open(rast_path)
        self.windowSide = windowSide
        self.windowNum = windowNum
        self.crs = rast.crs
        self.dtype = rast.dtypes[0]
        self.nodata = rast.nodata
        self.affine = rast.transform
		
        # Compute tile window
        horizontal = rast.width // windowSide + 1 - pow(0, (rast.width % windowSide))
        vertical = rast.height // windowSide + 1 - pow(0, (rast.height % windowSide))
			
        if (windowNum > horizontal * vertical) or (windowNum < 0):
            print("Window number outside bounds: " + str(self.windowNum))
            return None
		
        mod = windowNum % horizontal
        row = windowNum // horizontal - pow(0,mod)
        col = mod + pow(0,mod) * horizontal - 1

        if ((col + 1) * windowSide > rast.width):
            windowWidth = rast.width - (col * windowSide)
        else:
            windowWidth = windowSide
	        
        if ((row + 1) * windowSide > rast.height):
            windowHeight = rast.height - (row * windowSide)
        else:
            windowHeight = windowSide

        self.window = Window(col * windowSide, row * windowSide, windowWidth, windowHeight)
        rast.close()

			
    def loadTileFromWindowNum(self, rast_path):

        rast = rasterio.open(rast_path)

        array = rast.read(1, window=self.window)
        rast.close()

		# ! Check if there are no nulls - only works with integers !
        array_sub = np.subtract(array, self.nodata)
        if(np.sum(array_sub) == 0):
            print("Skipping tile without data: " + str(self.windowNum))
            exit()

        array_f = np.asarray(array, dtype=float)
        array_f[array_f == rast.nodata] = np.nan
        return array_f
	

    def getAffineTransform(self):
        # Returns New affine matrix for the tile with translation to the tile top left corner

        return Affine(
            self.affine.a,
            self.affine.b,
            self.affine.c + self.window.col_off * self.affine.a,
            self.affine.d,
            self.affine.e,
            self.affine.f + self.window.row_off * self.affine.e
        )

    
    def saveTile(self, array, path, dtype=None):

        if(dtype == None):
            dtype = self.dtype

        tile = rasterio.open(
            path,	
    		"w",
    		driver="GTiff",
            compress="DEFLATE",
            height=array.shape[0],
            width=array.shape[1],
    		count=1,
    		dtype=dtype, 
    		crs=self.crs,
    		transform=self.getAffineTransform(),
    		nodata=self.nodata
    	)
    
        tile.write(array, 1)
        tile.close()
  
