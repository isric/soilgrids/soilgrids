#!/bin/bash
# Runs the data preparation steps of SoilGrids. Before
# executing make sure all the variables have been set up correctly in the 
# config file(s)
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Spatial distribution analysis
$R_EXEC $SRCDIR/profiles/spatialDistribution.R





