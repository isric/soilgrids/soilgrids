-- Creates useful view to facilitate access to observations.
--
-- Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
-- Date: 18-02-2019
-- ############################################################################

----------- Join between profile and layer for ge-processing -----------
CREATE OR REPLACE VIEW sg250m.v_wosis_layer_profile AS 
SELECT p.geom,
	   p.latitude,
	   p.longitude,
	   l.*
  FROM sg250m.wosis_201901_profiles p,
       sg250m.wosis_201901_layers l
 WHERE l.profile_id = p.profile_id;