SELECT <voi>,
       depth_midpoint,
       easting,
       northing
  FROM observations.v_layer l,
       (SELECT ST_Union(geom) AS geom_set
          FROM public.countries_homolosine
         WHERE <cntr_col> LIKE '%<cntr_key>%') c
 WHERE id_stream = <stream>
   AND ST_Within(l.geom, c.geom_set) 
   AND <voi> is NOT NULL
   AND depth_midpoint > 0
   AND depth_midpoint < 200
