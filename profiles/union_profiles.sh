#!/bin/bash
# Reads records from the profile relation in PostGIS into GRASS. The selection
# of records is dictated by the data stream identifers set in the config file.
# The stream relation can be consulted for details on each data stream.
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 02-10-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

WHERE=""

for STREAM in `echo $DATA_STREAMS | tr "," " "`; do
	WHERE=$WHERE"id_stream="$STREAM" OR "
done
WHERE=${WHERE::-3}

# Import profile locations from observations schema
COMM=$GRASS_EXEC" "$GDIR"/"$GLOC"/"$MAPSET_USER"/ --exec "
COMM=$COMM"v.in.ogr input=\"PG:host="$PG_HOST" dbname="$PG_DB" port="$PG_PORT
COMM=$COMM" user=$PG_USER password=$PG_PASS\""
COMM=$COMM" layer="$PG_SCHEMA_OBS"."$REL_PROFILE
COMM=$COMM" columns=id,id_stream,id_dataset"
COMM=$COMM" output=profiles_snapshot"
COMM=$COMM" where=\""$WHERE"\" --overwrite -o"

eval $COMM

$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec db.execute "ALTER TABLE $PG_USER.profiles_snapshot ADD COLUMN cat INTEGER"
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec v.db.update profiles_snapshot col=cat qcol=id

# Union with synthetic profiles	
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec v.patch input=profiles_snapshot,$WISE_PROFILES,$SYNTH_VEC_PROFS out=$PROFILES_UNION --overwrite
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec db.execute "CREATE TABLE test AS SELECT * FROM profiles_snapshot UNION SELECT * FROM $WISE_PROFILES UNION SELECT * FROM $SYNTH_VEC_PROFS;"

# Remove temporary dataset	
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec g.remove type=vector name=profiles_snapshot -f


# $GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_USER} \
#     --exec v.in.ogr input="PG:host=$PG_HOST dbname=$PG_DB port=$PG_PORT user=$PG_USER password=$PG_PASS" \
#             layer=$PG_SCHEMA_OBS"."$REL_PROFILE output=profiles_snapshot \
#             where="id_stream=$DATA_STREAMS" -o
