SELECT <voi>,
       depth_midpoint,
       easting,
       northing
  FROM observations.v_layer 
 WHERE id_stream = <stream>
   AND <voi> is NOT NULL
   AND depth_midpoint > 0
   AND depth_midpoint < 200
   AND litter IS FALSE
