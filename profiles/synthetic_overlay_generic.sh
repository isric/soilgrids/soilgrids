#!/bin/bash
# Queries values of a given map at the locations of the synthetic mesh. This 
# script is meant to be run within a GRASS session.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 21-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

if [ -z "$1" ]
  then
    echo "Please provide the path to the map to query"
    exit 1
  else
    path="$1"
fi

if [ -z "$2" ]
  then
    echo "Please provide a name to the output dataset"
    exit 1
  else
    output="$2"
fi

g.region raster=${GREG}@PERMANENT res=$GRES

r.in.gdal input=$path output=imported_map

g.copy vect=$GLOBAL_MESH"@"$MAPSET_PROFS,$GLOBAL_MESH

db.execute "ALTER TABLE $PG_USER.$GLOBAL_MESH ADD COLUMN $output INTEGER"
v.what.rast map=$GLOBAL_MESH \
		    raster=imported_map \
		    column=$output \
		    --overwrite

# Remove nulls
v.extract input=$GLOBAL_MESH \
		  output=$output \
		  where="$output > 0" \
		  --overwrite 

v.db.update $output col=cat qcol="id+$SYNTH_ID_OFFSET"
v.db.update $output col=id qcol=cat

db.dropcolumn table=$GLOBAL_MESH column=$output -f



# These lines still do not function due to permissions
# g.mapset $MAPSET_PROFS
# g.copy vector=$output@$MAPSET_USER,$output --overwrite
