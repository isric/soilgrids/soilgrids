!/bin/bash
# Runs the data preparation steps of SoilGrids. Before
# executing make sure all the variables have been set up correctly in the 
# config file(s)
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Create global meash with Discrete Global Grid
$R_EXEC $SRCDIR/profiles/create_global_mesh.R

# ### Simulated profiles from WISE
$GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_USER} --exec $SRCDIR/profiles/synthetic_overlay_wise.sh

# Generic simulated profiles
$GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_USER} \
	--exec $SRCDIR/profiles/synthetic_overlay_generic.sh $SYNTH_MAP $SYNTH_VEC_PROFS

# Union with all Profiles
# # # source $SRCDIR/profiles/union_profiles.sh
# # # $GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_PROFS} \
# # # 	--exec g.copy vector=${PROFILES_UNION}@$MAPSET_USER,${PROFILES_UNION} --overwrite
$R_EXEC $SRCDIR/profiles/union_profiles.R

    
