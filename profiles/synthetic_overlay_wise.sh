#!/bin/bash
# Selects the locations of synthetic profiles within specifc land cover areas. 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 26-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Import WISE map
r.in.gdal input=$WISE_MAP output=WISE -o --overwrite

g.region raster=${GREG}@PERMANENT res=$GRES

db.execute "ALTER TABLE $PG_USER.$GLOBAL_MESH ADD COLUMN land_cover INTEGER"
v.what.rast map=$GLOBAL_MESH \
		    raster=LUC_ESA_LCE@COVS_CAT \
		    column=land_cover \
		    --overwrite
		        
v.extract input=$GLOBAL_MESH \
		  output=$WISE_PROFILES \
		  where="land_cover = 200" \
		  --overwrite 

# Overlay with WISE map
db.execute "ALTER TABLE $PG_USER.$WISE_PROFILES ADD COLUMN $WISE_ID INTEGER"
v.what.rast map=$WISE_PROFILES raster=WISE column=$WISE_ID --overwrite

# This cripples the WISE_PROFILES layer, union_profiles.sh fails after this.
v.db.update $WISE_PROFILES col=cat qcol=id

db.dropcolumn table=$PG_USER.$WISE_PROFILES column=land_cover -f
db.dropcolumn table=$PG_USER.$GLOBAL_MESH column=land_cover -f

# g.mapset $MAPSET_PROFS
# g.copy vector=$WISE_PROFILES@$MAPSET_USER,$WISE_PROFILES --overwrite




