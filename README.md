<a href="https://www.isric.org" rel="isric.org"> <img src="https://www.isric.org/themes/custom/basic/logo.svg"  height="130"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="https://soilgrids.org" rel="soilgrids.org"> <img src="https://www.isric.org/sites/default/files/styles/gallery_big_image_900x700/public/SoilGrids_banner_web.png"  height="130">

# SoilGrids v2.0

Global spatial predictions of soil properties at 250 m resolution

SoilGrids250m is developed and maintained by [ISRIC - World Soil Information](/isric/soilgrids/SoilGrids250m/-/blob/master/isric/soilgrids/SoilGrids250m/-/blob/master/www.isric.org). More information can be found at the [SoilGrids project website](https://www.isric.org/explore/soilgrids).

SoilGrids250m can be accessed [via the web portal](/isric/soilgrids/SoilGrids250m/-/blob/master/www.soilgrids.org), as [WMS/WCS layers](https://maps.isric.org/) or downloaded from [here](https://files.isric.org/soilgrids). Further details and tutorials on how to access the data can be found [here](https://www.isric.org/explore/soilgrids/faq-soilgrids#How_can_I_access_SoilGrids).

Citation:  
*Poggio, L., de Sousa, L. M., Batjes, N. H., Heuvelink, G. B. M., Kempen, B., Ribeiro, E., and Rossiter, D.: SoilGrids 2.0: producing soil information for the globe with quantified spatial uncertainty, SOIL, 7, 217–240, 2021.* [DOI](https://doi.org/10.5194/soil-7-217-2021)

The code in this repository is released under the [GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html) license according to the [ISRIC data policy](https://www.isric.org/about/data-policy). The code is provided without warranty. ISRIC is not obliged to provide user support, updates or “bug fixes” of any kind. For more details please see the [licence text](LICENCE).

For more information or collaboration opportunities please use the 'soilgrids at isric dot org' email address.

Copyright (c) 2019-2020 ISRIC - World Soil Information. All rights reserved. Any use of this software constitutes full acceptance of all terms of the document licence.
