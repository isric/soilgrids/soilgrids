#!/bin/bash
# Runs the full chain of scripts to compute SoilGrids. Before
# executing make sure all the variables have been set up correctly in the 
# config file(s)
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

export SRCDIR=$HOME/git/SoilGrids250m/
export CONFDIR=$HOME # directory where to put the config files project-wise
source 999_load_config.sh "name-soil-property" "name-RUN" 

# Optional: "name-RUN-predictions"

# Set region and resolution for each mapset used 
bash 000_main_envs_preparation.sh 
# ### To be run only once, to create GRASS mapsets and install r packages

# ### ### ### 
print_separator "ESDA"
time bash $SRCDIR/profiles/010_main_esda.sh
print_separator "Profile preparation"
time bash $SRCDIR/profiles/011_main_profile_preparation.sh
print_separator "Fold creation"
time bash $SRCDIR/cross-validation/020_main_fold_creation.sh
print_separator "Initial covariates reduction"
time bash $SRCDIR/covariates/030_main_init_covs_selection.sh
print_separator "Overlay"
time bash $SRCDIR/overlay/040_main_overlay.sh
print_separator "Creation of tiles"
time bash $SRCDIR/geo-processing/050_main_tiles_preparation.sh

# ### ### ### this is for each VOI
bash 002_dirs_creation.sh
# ### To be run for each soil properties (and changes in config file) to create directories and check that R packages are installed

print_separator "Data preparation"
time bash $SRCDIR/layers/060_main_layers_preparation.sh
print_separator "Covariates selection"
time bash $SRCDIR/covariates/070_main_covs_selection.sh
print_separator "Cross-validation"
time bash $SRCDIR/cross-validation/080_main_cross_valid.sh
print_separator "Model fit for predictions"
time bash $SRCDIR/models/090_main_model_fit.sh
print_separator "Predictions"
time bash $SRCDIR/predictions/100_main_predictions.sh

print_separator "Post-processing"
time bash $SRCDIR/predictions/110_main_post_processing.sh

print_separator "Creating VRTs" # 24 is indicatice: num depths * num quantiles
time bash $SRCDIR/predictions/121_main_create_vrt.sh

print_separator "Creating meta-data" 
time bash $SRCDIR/meta/130_main_meta-data.sh

# ### ### ### This is for post-processing (including uncertainty, not implemented in this branch)
time bash $SRCDIR/post-processing/200_main_post_processing.sh

# ### ### ### This is for reporting
time bash $SRCDIR/reporting/800_main_reporting.sh

