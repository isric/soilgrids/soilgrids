while true; do
    read -p "Do you wish to run model-fit via SLURM (s) or locally (l)? " answer
    case $answer in
        [Ss]* ) slurm=true; break;;
        [Ll]* ) slurm=false; break;;
        * ) echo "Please type in 's' for SLURM or 'l' for local.";;
    esac
done

NCORES=10

if [ "$slurm" = true ]; then
	# Create SLURM file
	slurm_folder=$MODDIR/slurm_logs
	if [ ! -d "$slurm_folder" ]; then
        mkdir -p $slurm_folder   
    fi 

	cat $SRCDIR/models/slurm_model_fit_template.sh | 
	    sed "s/JOB_NAME/\"$VOI"_modelfit"\"/g" | 
	    sed "s/MAIL/$MAIL/g" | 
	    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
	    sed "s/VOI/$VOI/g" |
            sed "s/TIME/03:00:00/g" | 
	    sed "s/RAMAVAIL/$RAMAVAIL/g" |
            sed "s/RUN/$RUN/g" |
            sed "s/RPRD/$RUNPRD/g" |
            sed "s/NNCORES/$NCORES/g" |
	    sed "s~SRC_PATH~$SRCDIR~g" > $slurm_folder/slurm_model_fit.sh

	echo "SLURM file created in: "$slurm_folder
	echo "Edit the file to ajust number of CPUs and set reservation node if needed."

else
	# Run full model-fit locally

	source $SRCDIR/models/091_run_model_fit.sh $NCORES
fi
