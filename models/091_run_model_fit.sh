
# use of $SRCDIR/models/$MODEL/fit_mod.R : number of cores, list of covariates selected by RFE, MTRY, Number of trees

NCORES=$1
t=1
for VAR in `echo $VOITR | tr "," " "`; do 
    mtry=`echo $MTRY | cut -d, -f$t`
    ntree=`echo $NTREE | cut -d, -f$t`

    $R_EXEC $SRCDIR/models/$MODEL/fit_mod.R $NCORES $COVS_VOI"_"$VAR"_RFE_final" $mtry $ntree $VAR

    let "t=t+1"
done


# ### If ALR transformation
if [ $TRANSFORM == "alr" ]; then
    $R_EXEC $SRCDIR/layers/data_transform/predict_back_alr_tree_select.R
fi

# ### variable importance
# This script uses: $VOI,$SRCDIR,$MODDIR and $PLOTDIR as input. VOI needs to be set with 999_load_config.sh. No arguments.
# $R_EXEC $SRCDIR/models/$MODEL/var_importance.R
