# Creates plots with feature importance for ranger models.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 17-06-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

library(ranger)
library(ggplot2)

source(paste0(Sys.getenv("SRCDIR"),"/common/ggplot_conf.R"))

plotIt <- function(svar)
{
    message(paste0("\nGoing to read: ", Sys.getenv("MODDIR"),svar,"_model.RDS"))
    mod=readRDS(paste0(Sys.getenv("MODDIR"),svar,"_model.RDS"))
    
    imp=data.frame(vars=names(mod$variable.importance),imp=mod$variable.importance,row.names=NULL)
    
    # Sort
    order.pop <- order(imp$imp, decreasing=TRUE)
    imp_ord <- imp[order.pop, ]
    imp_ord <- head(imp_ord,20)
    
    pp=ggplot(imp_ord, aes(x=reorder(vars,imp), y=imp,fill=imp)) + 
    	geom_bar(stat="identity", position="dodge")
    pp= pp+coord_flip() + ylab("") + xlab("") + guides(fill=F)
    pp= pp+ thggp
    pp= pp + ggtitle(Sys.getenv("VOI_FULL"))
    
    png(paste0(Sys.getenv("PLOTDIR"),"/importance_RF_",svar,".png"), 
    	width = 2000, height = 2000, units = 'px', res = 300)
    print(pp)
    dev.off()
}

vois_tr <- strsplit(Sys.getenv("VOITR"), ",")

if(length(vois_tr) > 0)
{
    lapply(vois_tr[[1]], plotIt)
} else {
    plotIt(Sys.getenv("VOI"))
}


