#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=JOB_NAME
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=MAIL
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=array_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment=PROJECT_CODE

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --constraint 32cpus
#SBATCH --time=02:00:00
#SBATCH --array=1
#SBATCH --cpus-per-task=10
#SBATCH --mem-per-cpu=RAMAVAIL

#-----------------------------Environment, Operations and Job steps----

module load gdal/gcc/64/2.2.2
module load geos/gcc/64
module load fftw3/openmpi/gcc/64
module load netcdf/gcc/64
module load gcc
module load glibc

export SRCDIR=SRC_PATH
source $SRCDIR/999_load_config.sh VOI RUN RPRD
 
source $SRCDIR/covariates/072_RFE_union.sh

