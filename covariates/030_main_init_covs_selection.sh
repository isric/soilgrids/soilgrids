#!/bin/bash
# Runs the initial covariates selection based on correlation coefficient threshold. 
# Before executing make sure all the variables have been set up correctly in the 
# config file(s)
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Script to give user the option to remove covariates that it does not like
# All covariates in the COVARIATES mapset
$GRASS_EXEC $GDIR/$GLOC/$USER/ --exec g.list rast pattern=* mapset=COVARIATES > $COVS_ALL"_init"

# Remove covariates based on expert opinion (if needed)
cat $COVS_ALL"_init" > $COVS_ALL"_overl"


# ### ### #1 --> correlation threshold
$R_EXEC $SRCDIR/covariates/covs_corr_sel.R $CORR_COEF
# $COVS_LIST"_corr_"$corr_coeff
