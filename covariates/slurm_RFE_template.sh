#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=JOB_NAME
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=MAIL
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=array_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment=PROJECT_CODE

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --constraint 32cpus
#SBATCH --time=TIME
#SBATCH --array=1-ARRAYNUM
#SBATCH --cpus-per-task=30
#SBATCH --mem-per-cpu=RAMAVAIL

#-----------------------------Environment, Operations and Job steps----

module load gdal/gcc/64/2.2.2
module load geos/gcc/64
module load fftw3/openmpi/gcc/64
module load netcdf/gcc/64
module load gcc
module load glibc

export SRCDIR=SRC_PATH
source $SRCDIR/999_load_config.sh VOIS RUN RPRD
 
FOLDS=`cat $PARAMDIR/seq_rfe.txt | sed "${SLURM_ARRAY_TASK_ID}q;d" | tr " " "," | sed 's/,*$//g'`

# # # for ct in in 0.95 0.85 0.75 0.65 0.50 0.25 0.15; do echo $ct
for VAR in `echo $VOITR | tr "," " "`; do
    $R_EXEC $SRCDIR/covariates/covs_rfe_select.R $COVS_ALL"_corr_"$CORR_COEF $FOLDS $VAR
done


