#!/bin/bash
# Runs steps 2 and 3 of the RFE process. It runs the RFE a second time only 
# with those covariates selected at least once in step 1 (RFE restricted to
# small sets of folds). Produces a final list of covariates for the VOI.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 05-06-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### step 2: Union of all selected covariates (at least once) 
for VAR in `echo $VOITR | tr "," " "`; do
 
    $R_EXEC $SRCDIR/covariates/post_covs_rfe_select.R $VAR

done


# ### step 3: Final RFE with all points
for VAR in `echo $VOITR | tr "," " "`; do
	# cat the csv with all covariuates selected produced in step 3 and subset to 
	# covariates appearing in all 4 sets of RFE run in step 1
    cat $RFEDIR/"df_rfe_sel_"$VAR".csv" | grep 4$ > $COVS_VOI"_"$VAR"_RFE_prelim0" 

    nc1=`cat $COVS_ALL"_corr_"$CORR_COEF | wc -l`
    nc2=`cat $COVS_VOI"_"$VAR"_RFE_prelim0" | wc -l`
    nc0=`echo "$nc1/3" | bc`
        
    # compare the length of the subset of covariates that appear in all folds 
    # with the length of the covariates selected by correlation coefficient
    if [ "$nc0" -gt "$nc2" ]; then
        # cat the csv with all covariuates selected produced in step 3
        covs_file=$RFEDIR/"df_rfe_sel_"$VAR".csv"               
    else
        covs_file=$COVS_VOI"_"$VAR"_RFE_prelim0"             
    fi
    
    # cat $CVDIR/"df_ref_sel_"$VAR".csv" | sed -e '1d' | wc -l
    cat $covs_file |                                     # 
    awk 'BEGIN { FS=OFS=","};{print $1}' |          # print only the column with covariates names
    sed -e '1d' |                                   # remove first row (header)
    sed 's/"//g' > $COVS_VOI"_"$VAR"_RFE_prelim"    # remove quotes and save to file

	# Run again RFE with all data and subset of covariates
    $R_EXEC $SRCDIR/covariates/covs_rfe_select.R $COVS_VOI"_"$VAR"_RFE_prelim" 0  $VAR 
done

# ### Preparation of list of covs
for VAR in `echo $VOITR | tr "," " "`; do
    cat $RFEDIR/"lst_rfe_predictors_"$VAR"_0.csv" | sed 's/"//g' > $COVS_VOI"_"$VAR"_RFE_final"
done

# ### Create parameter file (combinations of Mtry and number of trees) based on number of covariates selected from RFE
for VAR in `echo $VOITR | tr "," " "`; do
    $R_EXEC $SRCDIR/cross-validation/grid_hyperparameters.R $COVS_VOI"_"$VAR"_RFE_final" $MTRYS $NTREES $VAR
done
