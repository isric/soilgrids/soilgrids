#!/bin/bash
# Runs the covariates selection based on different methods. 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Correlation coefficient analysis in ./covariates/030_main_init_covs_selection.sh

# ### step 1: RFE using 3 folds (it will run four times)
source $SRCDIR/covariates/071_RFE.sh

# # # # ### step 2-3: RFE post-processing and finalisation of th COVS list
# # # source $SRCDIR/covariates/072_RFE_union.sh
