#!/bin/bash
# Runs the first step of the RFE process. RFE is applied on sets of 3 folds and
# records the covariates selected in each set. If necessary produces a SLURM 
# file for this step.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### Create text file with combinations of folds (e.g. 1,2,3-4,5,6-7,8,9-10,2,6). INT_RFE in config_model
$R_EXEC $SRCDIR/covariates/seq_create.R $INT_RFE

# ### step 1: RFE using 3 folds (it will run four times)

while true; do
    read -p "Do you wish to run RFE via SLURM (s) or locally (l)? " answer
    case $answer in
        [Ss]* ) slurm=true; break;;
        [Ll]* ) slurm=false; break;;
        * ) echo "Please type in 's' for SLURM or 'l' for local.";;
    esac
done

if [ "$slurm" = true ]; then
	# Create SLURM file
	NRUNS=`cat $PARAMDIR/seq_rfe.txt | wc -l`
	
	export slurm_folder=$MODDIR/slurm_logs
	if [ ! -d "$slurm_folder" ]; then
        mkdir -p $slurm_folder   
    fi 

	cat $SRCDIR/covariates/slurm_RFE_template.sh |
		sed "s/JOB_NAME/\"$VOI"_RFE"\"/g" | 
		sed "s/MAIL/$MAIL/g" | 
		sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
		sed "s/TIME/$PRED_TIME/g" | 
		sed "s/ARRAYNUM/$NRUNS/g" |
		sed "s/RAMAVAIL/$RAMAVAIL/g" |
		sed "s/VOIS/$VOI/g" |
		sed "s/RUN/$RUN/g" |
		sed "s/RPRD/$RUNPRD/g" |
		sed "s~SRC_PATH~$SRCDIR~g" > $slurm_folder/slurm_RFE.sh
		
	cat $SRCDIR/covariates/slurm_RFE_stage2_template.sh | 
		sed "s/JOB_NAME/\"$VOI"_RFE2"\"/g" | 
		sed "s/MAIL/$MAIL/g" | 
		sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
		sed "s/RAMAVAIL/$RAMAVAIL/g" |
		sed "s/VOI/$VOI/g" |
		sed "s/RUN/$RUN/g" |
		sed "s/RPRD/$RUNPRD/g" |
		sed "s~SRC_PATH~$SRCDIR~g" > $slurm_folder/slurm_RFE_stage2.sh
	
    

	echo "SLURM files created in: "$slurm_folder
	echo "Edit the file to ajust number of CPUs and set reservation node if needed."
	echo "After running SLURM continue the RFE by running 'sbatch slurm_RFE_stage2.sh'"
    echo "or by manually running the file 072_RFE_union.sh"

else
	# Run full RFE locally
	for VAR in `echo $VOITR | tr "," " "`; do
	    time(
	    cat $PARAMDIR/seq_rfe.txt | while read F; do 
		   	FOLDS=`echo $F | tr " " "," | sed 's/,*$//g'`
	    	$R_EXEC $SRCDIR/covariates/covs_rfe_select.R $COVS_ALL"_corr_"$CORR_COEF $FOLDS $VAR
	    done
	    )
	done
	
	source $SRCDIR/covariates/072_RFE_union.sh

fi







