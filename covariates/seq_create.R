nfolds=as.numeric(Sys.getenv("NUM_FOLDS"))
int=as.numeric(Sys.getenv("INT_RFE"))
main_seq=seq(1,nfolds,int)

set.seed(Sys.getenv("SETSEED"))
fout=paste0(Sys.getenv("PARAMDIR"),"seq_rfe.txt")

if(file.exists(fout)){file.remove(fout)}

for (i in 1:length(main_seq)){

    s=c(seq(main_seq[i],main_seq[i]+2),"\n")
    if(main_seq[i]==nfolds) {s=c(nfolds,sample(nfolds-1,2))}
    cat(s, file=fout, append=TRUE)
}
 cat("\n", file=fout, append=TRUE)
