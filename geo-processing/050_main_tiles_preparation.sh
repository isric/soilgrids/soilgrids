
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.mapset -c $MAPSET_TILES
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TILES/ --exec g.region rast=$GREG res=$GRES

$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TILES/ --exec bash $SRCDIR/geo-processing/tiling.sh $TILE_SIDE $GREG $TILE_PREF
