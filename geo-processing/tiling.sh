#!/bin/bash
# Creates a tiling system for parallelised predictions. This script is meant to
# be executed within a GRASS session, for instance:
# $ grass db/location/mapset 
# > . tiling.sh 364 4 ADMIN0
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Date: 22-01-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

start=`date +%s`

# Check arguments and set local variables
if [ -z "$1" ]
  then
	echo "Please provide the desired tile side in number of cells"
    exit 1
  else
	tileSide="$1"
fi

if [ -z "$2" ]
  then
    echo "Please provide the name of the land mask map"
    exit 1
  else
    landMask="$2"
fi

if [ -z "$3" ]
  then
    tilePref="tileSG"
  else
    tilePref="$3"
fi

# Compute the extent of the region
g.region raster=$landMask
eval `r.info -g $landMask`

let "tileSideMeters = tileSide * nsres"

echo "Tile size in bytes: $tileSize"
echo "Tile side in number of cells: $tileSide"
echo "Tile side in meters: $tileSideMeters"

# --- Compute entended extent ---

let "northSouth = north - south"
let "westEast = east - west"

echo "North-South width: $northSouth"
echo "East-West width: $westEast"

let "tilesNS = northSouth / tileSideMeters"
let "rem = northSouth % tileSideMeters"
if [ $rem -gt 0 ]
then
	let "tilesNS++"
fi
echo "Final tiles NS: $tilesNS"

let "tilesWE = westEast / tileSideMeters"
let "rem = westEast % tileSideMeters"
if [ $rem -gt 0 ]
then
	let "tilesWE++"
fi
echo "Final tiles EW: $tilesWE"

let "southExtended = north - tilesNS * tileSideMeters"
echo "Extended southern border: $southExtended"

let "eastExtended = west + tilesWE * tileSideMeters"
echo "Extended eastern border: $eastExtended"

let "totalTiles = tilesNS * tilesWE"
echo "Total number of tiles for the whole world: $totalTiles"

read -r -p "Do you wish to continue? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
   echo "Continuing to tile creation..."
else
   exit 0
fi

g.region e=$eastExtended s=$southExtended
echo "Region of computation: " 
g.region -p
r.mapcalc expression="tmp_region=1" --overwrite

# --- Build tiles for the whole region and rename them ---

r.tile input=tmp_region output=$tilePref width=$tileSide height=$tileSide --overwrite 

for t in `g.list rast pattern=$tilePref"*"`; do
	echo ""
    segments=(${t//-/ })
    tileNum=`expr ${segments[1]} \* 1000 + ${segments[2]}`
    g.region raster=$t
    exp="\""$t"\""="\""$t"\""*$tileNum+$landMask/$landMask
    echo "Creating tile with expression: $exp"
	r.mapcalc expression=$exp --overwrite
	eval `r.info -r map=$t`
	echo "$t min: $min"
	if [ $min = NULL ] 
	then
		echo "Removing empty tile: $t"
		g.remove type=raster name=$t -f
	fi
done

g.region raster=tmp_region
g.list rast pattern=$tilePref-* > $TILE_LIST
r.buildvrt file=$TILE_LIST output=$tilePref"_patch" --overwrite
r.colors map=$tilePref"_patch" color=random 

# Clean up
g.remove pattern=tmp_* type=raster -f

end=`date +%s`
runtime=$((end-start))
echo "Total runtime: $runtime seconds"
