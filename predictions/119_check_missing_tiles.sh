# NOTE: it requires a template created from a VRT with all the subtiles. See docs for an example (missing_tiles.md)

if [ -f $PARAMDIR/"tile_list_rerun_dpts.txt" ]; then
    rm $PARAMDIR/"tile_list_rerun_dpts.txt"
fi

for dpt in "0-5cm" "5-15cm" "15-30cm" "30-60cm" "60-100cm" "100-200cm"; do
    for voi in $(echo $VOI | tr "," "\n"); do 
        # Options: 
        if [ "$typost" = "" ]; then 
            file=$PREDIR/tiff/$voi"_"$dpt"_mean.vrt"
        elif [ "$typost" = "uncertainty" ]; then
            file=$MODDIR/uncertainty/$RUNPRD/tiff/$voi"_"$dpt"_uncertainty.vrt"
        elif [ "$typost" = "texture" ]; then
            file=$MODDIR/texture/$RUNPRD/tiff/texture_USDA_$dpt".vrt"
        else
            echo "???"
        fi


        gdalinfo $file | grep tiff > pred_subtiles.txt.tmp
        cat pred_subtiles.txt.tmp | rev | cut -d/ -f1 | rev | cut -d. -f1 > pred_subtiles.txt

        diff $INDIR"/TILES/subtiles_"$TILE_SIDE"_"$SUB_TILES"x"$SUB_TILES".txt" pred_subtiles.txt | grep \< | sed 's/< //g' | cut -d_ -f1 | uniq > $PARAMDIR/"tile_list_rerun.txt"
        cat $PARAMDIR/"tile_list_rerun.txt" >> $PARAMDIR/"tile_list_rerun_dpts.txt"
        
        tile_list="tile_list_rerun.txt"
        tts_len=`cat $PARAMDIR/$tile_list | wc -l`
        echo $dpt "for" $voi "has" $tts_len "tiles missing"
        
    done
done

if [ "$VOI" = "sand,silt,clay" ]; then
    cat $PARAMDIR/"tile_list_rerun_dpts.txt" | sort | uniq > $PARAMDIR/"tile_list_rerun_dpts.txt0"
    mv $PARAMDIR/"tile_list_rerun_dpts.txt0" $PARAMDIR/"tile_list_rerun_dpts.txt"
fi
