#!/bin/bash
# Creates VRT for a given prediction output, e.g. a depth interval for a 
# specific property. In the HPC cluster the GDAL library must be loaded 
# beforehand, e.g.:
#
# module load gdal/gcc/64/2.2.2
#
# The programme that adds meta-data records relies on the GDAL library for 
# Python. Therefore a Python virtual environment must be activated before 
# running. Details on Python in Anunna are available in the Wiki:
# https://wiki.anunna.wur.nl/index.php/Setting_up_Python_virtualenv
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 22-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

if [ -z "$1" ]
then
  echo "Please provide a folder name to process"
  exit 1
fi

folder=`sed "$1q;d" folders.txt`

echo "Creating VRT and overviews for map: "$folder
find ./$folder -name "*.tif" > $folder".txt"
gdalbuildvrt -input_file_list $folder".txt" -overwrite $folder.vrt -tr 250 250
gdaladdo -ro --config COMPRESS_OVERVIEW DEFLATE \
		--config BIGTIFF_OVERVIEW YES $folder.vrt 4 16 64 256	
rm $folder".txt"
echo "Completed VRT for: "$folder

# Add meta-data

if [ $TRANSFORM == "alr" ]; then
    covariates=""
    for TR in 1 2 
    do    
        trcovs=`cat ${COVS_VOI}_alr${TR}_RFE_final | paste -sd "," -`
        TRcovs=`echo $TR | tr a-z A-Z`":"$trcovs
        covariates=$covariates","$TRcovs
    done
    covariates=`echo $covariates | sed 's/^,//'`
else
    covariates=`cat ${COVS_VOI}_${VOI}_RFE_final | paste -sd "," -`
fi

if [ $VOI == "sand,silt,clay" ]; then
    rec=`cat ${SRCDIR}/docs/runs_log.csv | grep ${VOI} | grep ${RUN}`
    IFS=','; arrIN=($rec); unset IFS;
    description=${arrIN[9]}
else
    description=`cat ${SRCDIR}/docs/runs_log.csv | 
               grep ${VOI} | grep ${RUN} | cut -d, -f 8- | 
               awk -F"\"," 'BEGIN {OFS=FS} {print $1}' | sed 's/^"//' `
fi

echo "Adding meta-data to "${folder}.vrt 

gdal_edit.py -mo WoSIS_version="Data stream "$DATA_STREAMS \
    -mo Litter_layers=$USE_LITTER \
    -mo Other_synth_profiles=${SYNTH_MAP##*/} \
    -mo Covariates=$covariates \
    -mo Transformation=$TRANSFORM \
    -mo Model="Quantile Regression Forests" \
    -mo Model_type=$MODEL \
    -mo Number_trees=$NTREE \
    -mo Mtry=$MTRY \
    -mo Mapped_units=$META_UNITS \
    -mo Outputs_version=$RUN \
    -mo Outputs_abstract="${description}" \
    -mo Quantile=${fileArr[2]%.vrt} \
    -mo Depth_interval=${fileArr[1]} \
    -mo Code_version=$VERS \
    ${folder}.vrt

if [ ! -z "$WISE_VOI" ]
then
    echo "WISE VOI: "$WISE_VOI
    gdal_edit.py -mo WISE_version=${WISE_MAP##*/} ${folder}.vrt
fi
