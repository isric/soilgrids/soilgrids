#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=JOB_NAME
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=MAIL
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=vrt_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment=PROJECT_CODE

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --constraint 32cpus
#SBATCH --time=15
#SBATCH --array=1-NUMFOLDERS
#SBATCH --mem-per-cpu=RAMAVAIL

#-----------------------------Environment, Operations and Job steps----

module load gdal/gcc/64/2.2.2
module load geos/gcc/64
module load fftw3/openmpi/gcc/64
module load glibc
module load gcc

source SRC_PATH/999_load_config.sh VOI RUN RPRD
source $PY_ENV

cd $PREDIR/tiff

bash $SRCDIR/predictions/create_vrt.sh ${SLURM_ARRAY_TASK_ID}
