#!/bin/bash
# Creates SLURM file and necessary output folders to create VRTs.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 24-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# List folders to process
cd $PREDIR/tiff
for i in $(ls -d */); do echo ${i%%/}; done > folders.txt
NUMFOLDERS=`wc -l < folders.txt`

# ### Create slurm file

U_VOI=`echo $VOI | tr , _`

# NOTE: using VOI below, possibly to modify for compositional variables
cat $SRCDIR/predictions/slurm_create_vrt_template.sh | 
    sed "s/JOB_NAME/\"$VOI"_VRT"\"/g" | 
    sed "s/MAIL/$MAIL/g" | 
    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
    sed "s/TIME/$VRT_TIME/g" | 
    sed "s/CPU/$1/g" |
    sed "s/RAMAVAIL/$VRT_RAM/g" |
    sed "s/VOI/$U_VOI/g" |
    sed "s/RUN/$RUN/g" |
    sed "s/RPRD/$RUNPRD/g" |
    sed "s/NUMFOLDERS/$NUMFOLDERS/g" |
    sed "s~SRC_PATH~$SRCDIR~g" > $SLURM_PATH/slurm_create_vrt.sh

cd $PREDIR/slurm_logs

echo "Created SLURM file in: "$SLURM_PATH
echo "Check parameters in SLURM before running (e.g. reserved node)."
echo "Make sure the path to the Python virtual environment is correct."






