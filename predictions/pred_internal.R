mod=readRDS(paste0(Sys.getenv("MODDIR"),svar,"_model.RDS"))
covs=mod$forest$independent.variable.names # NOTE: model dependent

message(paste0("Loading covariates for tile ", tile))
covs_rst=toupper(covs[grep(Sys.getenv("DEPTH_COL"),covs,invert=TRUE)]) # get all the covariates we need to read from GRASS (excluding depth)



rst0=raster(readRAST(Sys.getenv("LAND_MASK")))
if(all(is.na(values(rst0)))){ 
        message(paste0("Empty subtile ", tile)); 
        sink(paste0(Sys.getenv("SLURM_PATH"),"/list_tiles/list_tiles_",args[1],".txt"), append=TRUE)
        cat(paste0(tile, " not covariate"))
        sink()
quit(save="no") }

rst=brick(lapply(covs_rst,function(x){
		message(paste0("Loading raster ", x))
		raster(readRAST(x))}))
rst=addLayer(rst0,rst)
names(rst)=tolower(names(rst))
rm(rst0)

crs_rst=proj4string(rst)
		
df_pred=as.data.frame(rst,xy=TRUE)
df_pred=df_pred[complete.cases(df_pred),]

# Check if the sub-tile contains land
if(nrow(df_pred) == 0) { 
        message(paste0("Empty subtile ", tile)); 
        sink(paste0(Sys.getenv("SLURM_PATH"),"/list_tiles/list_tiles_",args[1],".txt"), append=TRUE)
        cat(paste0(tile, " not covariate"))
        sink()
quit(save="no") }

# function to be applied to predict and save geotiff. TODO: argument for GRASS?
source(paste0(Sys.getenv("SRCDIR"),"/models/",Sys.getenv("MODEL"),"/predict_grids.R"))
source(paste0(Sys.getenv("SRCDIR"),"/predictions/","/write_raster.R"))

message(paste0("Starting prediction for tile ", tile, " ..."))

if(Sys.getenv("MOD2D")==TRUE){
        predictions=fun_pred_qrf(data=df_pred, model=mod);
                
} else {
        predictions=lapply(1:length(depths_mids),function(x){
                fun_pred_qrf_dpt(data=df_pred,
                            model=mod,
                            depthMid=depths_mids[x]);
                })
}
