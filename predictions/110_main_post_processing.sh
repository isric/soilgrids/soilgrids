

# rsync -ah --info=progress2 PATH_ON_LUSTRE/tiff/* PATH_ON_NFS/VARIABLE --dry-run
rsync -ah --info=progress2 ./projects/soilgrids/hpc/SG2019/v2.0.0/outputs/cfvo/ranger_RUN02_WISE/predict/RUN02_WISE/tiff/ ./isric_data/soilgrids/versions/2019-06-20/cfvo/ --dry-run
rsync -ah --progress ./projects/soilgrids/hpc/SG2019/v2.0.0/outputs/cfvo/ranger_RUN02_WISE/predict/RUN02_WISE/tiff/ ./isric_data/soilgrids/versions/2019-06-20/cfvo/ --dry-run

rsync -ah --progress ./projects/soilgrids/hpc/SG2019/v2.0.0/outputs/sand_silt_clay/ranger_RUN02_WISE/predict/RUN03/tiff/sand* ./isric_data/soilgrids/versions/2019-06-20/sand/ --dry-run


rsync -rlDv --no-perms --stats --checksum --omit-dir-times --progress
