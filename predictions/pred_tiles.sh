#!/bin/bash
# Sets up the GRASS environment for the prediction of a single tile. 
# Sub-divides the tile in case the SUB_TILE variable is bigger than 1.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 17-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

if [ -z "$1" ]
  then
    echo "Please provide the tile name"
    exit 1
  else
    tile="$1"
fi

if [ ! -d "$PREDIR/tiff/" ]; then
  mkdir -p $PREDIR/tiff/
fi

$GRASS_EXEC $GDIR/$GLOC/$USER -f --exec g.mapset -c $MAPSET_TMP$tile # this create a new mapset with name pattern TMP_name_tile (passed from previous bash script)

$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec g.mapsets $MAPSET_COVS,$MAPSET_TILES operation="add"  # add necessary mapsets to the path
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec g.region rast=$tile res=$GRES -a # set the region to the tile 

$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec g.region -p
eval `$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec r.info -g $tile`

bottom=$north
for (( stv=1; stv<=$SUB_TILES; stv++ )) do
    top=$bottom
    let "bottom=$top-($cols/$SUB_TILES)*$nsres"
    
    right=$west
    for (( sth=1; sth<=$SUB_TILES; sth++ )) do
        left=$right
        let "right=$left+($rows/$SUB_TILES)*$ewres"

        $GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec g.region n=$top s=$bottom e=$right w=$left
        $GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec r.mapcalc "ttt = if(GAUL_ADMIN0_landmask_Homolosine@PERMANENT > 0,1,null())" --overwrite
        eval `$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec r.info -s ttt` # > $SLURM_PATH/subtile.txt
        # Use eval instead of text file on disk
        
        if [ "$sum" == "NULL" ]; then      
#         if grep -q sum=NULL $SLURM_PATH/subtile.txt; then
            echo $tile"_"$stv-$sth "not land" >> $SLURM_PATH/list_tiles/list_tiles_$tile.txt

        else
#             echo $tile"_"$stv-$sth "land" >> $SLURM_PATH/list_tiles/list_tiles_$tile.txt
            $GRASS_EXEC $GDIR/$GLOC/$MAPSET_TMP$tile --exec $R_EXEC $SRCDIR/predictions/pred_script.R $tile "$stv-$sth"
        fi
        
    done

done
# Fix permissions
# chmod -R g+rw $GDIR/$GLOC/$MAPSET_TMP$tile
rm -Rf $GDIR/$GLOC/$MAPSET_TMP$tile
