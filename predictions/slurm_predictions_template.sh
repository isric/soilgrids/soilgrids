#!/bin/bash
# -----------------------------Name of the job-------------------------
#SBATCH --job-name=JOB_NAME
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=MAIL
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=array_%A-%a.out
#-----------------------------Other information------------------------
#SBATCH --comment=PROJECT_CODE

#-----------------------------Required resources-----------------------
#SBATCH --partition=main
#SBATCH --qos=std
#SBATCH --constraint 32cpus
#SBATCH --time=TIME
#SBATCH --array=ARRAYNUM
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=RAMAVAIL

#-----------------------------Environment, Operations and Job steps----

module load gdal/gcc/64/2.2.2
module load geos/gcc/64
module load fftw3/openmpi/gcc/64
module load glibc
module load gcc
# Uncomment below to use the shared version of R
# module load R/3.5.0

source SRC_PATH/999_load_config.sh VOI RUN RPRD

tile=`cat $TILE_LIST | sort | sed -n "${SLURM_ARRAY_TASK_ID}p"`

export TMPDIR=$PREDIR
bash SRC_PATH/predictions/pred_tiles.sh $tile 
