cat $TILE_LIST

find $PREDIR/tiff/$VOI"_0-5cm_mean" -type d | rev | cut -d'/' -f 1 | rev | sort > $PARAMDIR/"computed_tiles_"$VOI".txt"

diff $TILE_LIST $PARAMDIR/"computed_tiles_"$VOI".txt" | grep \< | sed 's/< //g' > $PARAMDIR/"tile_list_rerun.txt"
