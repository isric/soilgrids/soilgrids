#!/bin/bash
# Verifies prediction outputs.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 21-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Gather error messages
grep -rnis "err" $SLURM_PATH/*.out > errors.txt

# Count number of tiff files created
find $PREDIR/tiff -type f | wc -l
