if test -f "$SLURM_PATH/list_tiles/list_tiles.txt"; then
   rm $SLURM_PATH/list_tiles/list_tiles.txt
fi

for f in $SLURM_PATH/list_tiles/*; do echo $f
    cat $f | sed '/not/d' | cut -d " " -f1 >> $SLURM_PATH/list_tiles/list_tiles.txt
done

cat $SLURM_PATH/list_tiles/list_tiles.txt | sort > $PARAMDIR/"land_subtiles_"$VOI".txt"

# ### find tiles computed for one of the depth and one of the "quantiles". TODO: check in all folders to see if there are more missing?
find $PREDIR/tiff/$VOI"_0-5cm_mean" -type f -exec sh -c 'echo $(basename {}) | cut -d . -f1' \; > $PARAMDIR/"computed_subtiles_tmp.txt"
cat $PARAMDIR/"computed_subtiles_tmp.txt" | sort > $PARAMDIR/"computed_subtiles_"$VOI".txt"
rm $PARAMDIR/"computed_subtiles_tmp.txt"

# ### Differnece between the $TILE_LIST (i.e. all tiles created) and the tiles that have at least one geotiff in one of the sub-tiles.
diff $PARAMDIR/"land_subtiles_"$VOI".txt" $PARAMDIR/"computed_subtiles_"$VOI".txt" | grep \< | sed 's/< //g' > $PARAMDIR/"subtile_list_rerun.txt"
cat $PARAMDIR/"subtile_list_rerun.txt" | cut -d _ -f 1 | sort | uniq > $PARAMDIR/"tile_list_rerun.txt"


# TODO: list of not existing sub-tiles in $SLURM_PATH/list_tiles/
# TODO: list of produced tiff in $PREDIR/tiff/
# TODO: max number of sub-tiles $SUB_TILES*$SUB_TILES
# TODO: identify which ones are missing


# ### create slurm file to run the missing tiles.
# tile_list=slurm_predict_error.txt
tile_list="tile_list_rerun.txt"
tts_len=`cat $PARAMDIR/$tile_list | wc -l`


cat $SRCDIR/predictions/slurm_predictions_template.sh | 
    sed "s/JOB_NAME/\"$VOI"_pred"\"/g" | 
    sed "s/MAIL/$MAIL/g" | 
    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
    sed "s/TIME/$PRED_TIME/g" | 
    sed "s/ARRAYNUM/1-$tts_len/g" |
    sed "s/RAMAVAIL/$RAMAVAIL/g" |
    sed "s/VOI/$VOI/g" |
    sed "s/RUN/$RUN/g" |
    sed "s/RPRD/$RUNPRD/g" |
    sed "s~SRC_PATH~$SRCDIR~g" |
    sed "s~TILE_LIST~PARAMDIR\/\"$tile_list\"~g" > $SLURM_PATH/slurm_predict_missing.sh
    

    

mkdir job_18314500
mv array_18314500-* ./job_18314500/




