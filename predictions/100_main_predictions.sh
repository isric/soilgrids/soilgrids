#!/bin/bash
# Creates SLURM file and necessary output folders for predictions.
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 24-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Save as text file in PARAMDIR
# ### Create slurm file
$GRASS_EXEC $GDIR/$GLOC/$USER --exec g.list type=rast mapset=$MAPSET_TILES pattern=$TILE_PREF-* > $TILE_LIST
tts_len=`cat $TILE_LIST | wc -l`

if [ ! -d "$SLURM_PATH" ]; then
  mkdir -p $SLURM_PATH
fi

# NOTE: using VOI below, possibly to modify for compositional variables
cat $SRCDIR/predictions/slurm_predictions_template.sh | 
    sed "s/JOB_NAME/\"$VOI"_pred"\"/g" | 
    sed "s/MAIL/$MAIL/g" | 
    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
    sed "s/TIME/$PRED_TIME/g" | 
    sed "s/ARRAYNUM/1-$tts_len/g" |
    sed "s/RAMAVAIL/$RAMAVAIL/g" |
    sed "s/VOI/$VOI/g" |
    sed "s/RUN/$RUN/g" |
    sed "s/RPRD/$RUNPRD/g" |
    sed "s~SRC_PATH~$SRCDIR~g" > $SLURM_PATH/slurm_predictions.sh

# NOTE: sed "s/ARRAY/1-$tts_len%MAX_NUMBER_OF_JOBS_AT_EACH_TIME/g" |
# NOTE: sed "s/ARRAY/1-$tts_len:RUN_EVERY_X_JOB/g" |


echo "Created SLURM file in: "$SLURM_PATH
echo "Check parameters in SLURM before running (e.g. reserved node)"

# NOTE: run SLURM file at this point

# Example of how to run without SLURM
# export tile="something"
# $SRCDIR/predictions/pred_tiles.sh  $tile # SLURM or semaphore if needed


# back-transformation
# $SRCDIR/predictions/back_transformation.R



