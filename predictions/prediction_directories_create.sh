# Create sub-directories for saving geotiffs 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 17-05-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### create folders for geotiffs

depths=`echo $DPTINT | tr "," " "`
len_depths=`echo $depths | wc -w`

for (( d=1; d<=$((len_depths - 1)); d++ )) do echo $d

    dpt=`echo $depths | cut -d" " -f$d,$((d+1)) | tr " " "-"`"cm"
    
    for Q in `echo $QUANTS | tr "," " "`; do 
        mkdir -p $PREDIR/tiff/$VOI"_"$dpt"_Q"$Q
        chmod -R g+rws $PREDIR/tiff/$VOI"_"$dpt"_Q"$Q
    done
        mkdir -p $PREDIR/tiff/$VOI"_"$dpt"_mean"
		chmod -R g+rws $PREDIR/tiff/$VOI"_"$dpt"_mean" 
done
