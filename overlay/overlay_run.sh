g.region rast=$GREG res=$GRES
g.mapsets "COVARIATES" operation="add"

# ### ### Copy tables $PROFILES_OVERLAY into user mapset
eval `g.findfile element=vector file=$PROFILES_OVERLAY`
if [ -z "$name" -o $mapset != $MAPSET_OVER ]
  then
        g.copy vector=$PROFILES_OVERLAY"@"$MAPSET_PROFS,$PROFILES_OVERLAY
fi


# add x and y to the vectors
v.to.db map=$PROFILES_OVERLAY option=coor columns=x,y

# ### check which profiles are already existing and, if necessary, create new table with missing profiles
# Not working. See issue # 253
export VCT_OVERL=$PROFILES_OVERLAY

# ### check which covariates were already sampled and, if necessary, create new list with missing covariates
if db.tables | grep $USER"."$VCT_OVERL"_covs" --quiet ; then
  db.columns $VCT_OVERL > $COVSDIR/$USER"_covs_list_exist.txt"
  g.list rast pattern=* mapset=COVARIATES > $COVSDIR/$USER"_covs_list_add.txt"

  diff $COVSDIR/$USER"_covs_list_exist.txt" $COVSDIR/$USER"_covs_list_add.txt" | grep \> | sed 's/> //g' > $COVSDIR/$USER"_covs_list_add_overl.txt"
  covs_list_overl=$COVSDIR/$USER"_covs_list_add_overl.txt"
else
  covs_list_overl=$COVSDIR$COVS_OVERL
fi

dbdrv=`db.connect -p | grep driver | cut -d: -f2`

time (
cat $covs_list_overl | while read cov; do echo $cov
    coln=`echo "$cov" | tr '[:upper:]' '[:lower:]'`
    
        if [[ $dbdrv == " pg" ]]; then
            db.execute "ALTER TABLE $PG_USER.$VCT_OVERL ADD COLUMN $coln FLOAT"
        fi

    v.what.rast map=`echo $VCT_OVERL | cut -d @ -f1` raster=$cov column=$coln
done
)

g.rename vect=$PROFILES_OVERLAY,$PROFILES_COVS
