#!/bin/bash
# Runs the overlay. 
# Before executing make sure all the variables have been set up correctly in the 
# config file(s)
#
# To log the output of execution to a file use a command like:
# bash ./main.sh > out.log 2>&1
# tail -f out.log
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### ### ### Overlay
$GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_OVER} --exec bash $SRCDIR/overlay/overlay_run.sh   


while true; do
    read -p "Do you want to copy the overlay to the shared mapset? WARNING: This will overwrite existing file" answer
    case $answer in
        [Yy]* ) copyvect=true; break;;
        [Nn]* ) copyvect=false; break;;
        * ) echo "Please type in 'y' if you want to copy.";;
    esac
done

if [ "$copyvect" = true ]; then

$GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_PROF} --exec g.copy $PROFILES_COVS"@"$MAPSET_OVER,$PROFILES_COVS

fi
