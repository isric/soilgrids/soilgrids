#!/bin/bash
# prepare the environment. 
# Before executing make sure all the variables have been set up correctly in the 
# config file(s)
#
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################
$GRASS_EXEC -c $WDIR/countries/countries_homolosine.gpkg -e $GDIR/$GLOC/

$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec v.in.ogr input=$WDIR/countries/countries_homolosine.gpkg output=countries_homolosine
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec r.in.gdal input=$WDIR/countries/GAUL_ADMIN0_landmask_Homolosine.tif output=GAUL_ADMIN0_landmask_Homolosine
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec v.in.ogr input=$WDIR/countries/CounterDomain.geojson output=CounterDomain -o
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec v.in.ogr input=$WDIR/countries/ParallelsMeridians.geojson output=ParallelsMeridians -o
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.region rast=$GREG res=$GRES
# $GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.region -s # TODO totest

$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.mapset -c $MAPSET_COVS
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_COVS/ --exec g.region rast=$GREG res=$GRES
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.mapset -c $MAPSET_TILES
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_TILES/ --exec g.region rast=$GREG res=$GRES
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.mapset -c PROFILES # link with postgis DB (see readin_postgis.sh)
$GRASS_EXEC $GDIR/$GLOC/PROFILES/ --exec g.region rast=$GREG res=$GRES
$GRASS_EXEC $GDIR/$GLOC/PERMANENT/ --exec g.mapset -c $MAPSET_PROFS
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_PROFS/ --exec g.region rast=$GREG res=$GRES
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec g.region rast=$GREG res=$GRES
$GRASS_EXEC $GDIR/$GLOC/$MAPSET_USER/ --exec g.mapsets $MAPSET_PROFS,$MAPSET_COVS operation=add


$R_EXEC $SRCDIR/001_install_pcks.R
