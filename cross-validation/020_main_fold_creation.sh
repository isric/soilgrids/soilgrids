#!/bin/bash
# Runs the fold creation for cross-validation et al. 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# Create folds with spatial stratification
$R_EXEC $SRCDIR/cross-validation/spatial_stratification.R

# Copy profiles to main mapset
$GRASS_EXEC ${GDIR}/${GLOC}/${MAPSET_PROFS} \
	--exec g.copy vector=${PROFILES_FOLD}@${MAPSET_USER},${PROFILES_FOLD} --overwrite 
# TODO: move profiles_fold as profiles_folds (permission problems)

# clean_data.R
# remove observations with NA for the variable of interest (VOI)

# ./join_overlay.R
