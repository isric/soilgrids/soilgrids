#!/bin/bash
# Cross-validation post processing. 
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 05-06-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# DANGER: to finalise below
# ### Append files
for VAR in `echo $VOITR | tr "," " "`; do
	$R_EXEC $SRCDIR/cross-validation/post_append.R $VAR
	$R_EXEC $SRCDIR/cross-validation/post_append_summary.R $VAR
done
# TODO: delete single files


# ### Post processing for back-transformation
source $SRCDIR"/layers/data_transform/cross_valid_back_"$TRANSFORM".sh"


# ### ### Analyse results
# see reporting

# NOTE: update config/config_voi.sh with appropriate values of mtry and ntree
# echo 12,150 > $PARAMDIR/model_hyperparams.txt # To be used as input in models/090_main_model_fit.sh

echo "Cross-validation completed. Before proceeding to predictions, update the model parameters in config_voi.sh (e.g. MTry and number of trees for Random Forests)."
echo "And do not forget to reload the configuration."
