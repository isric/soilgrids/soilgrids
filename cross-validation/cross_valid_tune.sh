for VAR in `echo $VOITR | tr "," " "`; do

    cat $PARAMDIR/"grid_hyperparameters_"$VAR".csv" | while read line; do echo $line 
        mtry=`echo $line | cut -d, -f1`
        ntree=`echo $line | cut -d, -f2`

            $R_EXEC $SRCDIR/cross-validation/cross_validation.R $COVS_VOI"_"$VAR"_RFE_final" $1 $mtry $ntree $VAR
            
    done    
done

# TODO: full testing for ALR
# if [ $TRANSFORM != "" ]; then
#     echo "Back-transformation"
#     
# source $SRCDIR"/layers/data_transform/cross_valid_back_"$TRANSFORM".sh"  
#     
#     
# fi
