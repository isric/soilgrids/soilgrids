#!/bin/bash
# Runs the cross-validation. 
#
# To log the output of this script into a file use a formulation like:
# echo "l" | source 080_main_cross_valid.sh > $CVDIR/out.log 2>&1
#
# Author: Luís de Sousa luis (dot) desousa (at) isric (dot) org
# Author: Laura Poggio laura (dot) poggio (at) isric (dot) org
# Date: 07-03-2019
# 
# Copyright (c) 2019 ISRIC - World Soil Information. All rights reserved.
# Any use of this software constitutes full acceptance of all terms of the
# document licence.
# SPDX-License-Identifier:  
###############################################################################

# ### ### Model tuning and cross-validation (after RFE)
# Arguments grid_hyperparameters.R: list of covariates, multiplier for sqrt(number_of_covariates), vector with the number of trees to be tested
# Arguments cross_validation.R: number of CPUs, list of covariates, fold, mtry, ntree, soil property

# NOTE The parameter file (combinations of Mtry and number of trees) is created at the end of RFE 072_RFE_union.sh

while true; do
    read -p "Do you wish to run cross-validation via SLURM (s) or locally (l)? " answer
    case $answer in
        [Ss]* ) slurm=true; break;;
        [Ll]* ) slurm=false; break;;
        * ) echo "Please type in 's' for SLURM or 'l' for local.";;
    esac
done

if [ "$slurm" = true ]; then
	# Create SLURM file
	slurm_folder=$MODDIR/slurm_logs
	if [ ! -d "$slurm_folder" ]; then
        mkdir -p $slurm_folder   
    fi 

	cat $SRCDIR/cross-validation/slurm_cross_valid_template.sh | 
	    sed "s/JOB_NAME/\"$VOI"_CV"\"/g" | 
	    sed "s/MAIL/$MAIL/g" | 
	    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
	    sed "s/RAMAVAIL/$RAMAVAIL/g" |
	    sed "s/VOI/$VOI/g" |
        sed "s/RUN/$RUN/g" |
        sed "s/RPRD/$RUNPRD/g" |
	    sed "s~SRC_PATH~$SRCDIR~g" > $slurm_folder/slurm_cross_valid.sh
	    
	cat $SRCDIR/cross-validation/slurm_cross_valid_stage2_template.sh | 
	    sed "s/JOB_NAME/\"$VOI"_CV2"\"/g" | 
	    sed "s/MAIL/$MAIL/g" | 
	    sed "s/PROJECT_CODE/\'$PROJ_CODE\'/g" | 
	    sed "s/RAMAVAIL/$RAMAVAIL/g" |
	    sed "s/VOIS/$VOI/g" |
	    sed "s/TIME/01:00:00/g" | 
        sed "s/RUN/$RUN/g" |
        sed "s/RPRD/$RUNPRD/g" |
	    sed "s~SRC_PATH~$SRCDIR~g" > $slurm_folder/slurm_cross_valid_stage2.sh

	echo "SLURM file created in: "$slurm_folder
	echo "Edit the file to ajust number of CPUs and set reservation node if needed."
	echo "After running SLURM continue the post-processing by running 'sbatch slurm_RFE_stage2.sh'"
	echo "or by manually running the file 082_post_cross_valid.sh"

else
	# Run full cross-validation locally
	for (( F=1; F<=$NUM_FOLDS; F++ )) do
	    source $SRCDIR/cross-validation/cross_valid_tune.sh $F
	done

	source $SRCDIR/cross-validation/082_post_cross_valid.sh
fi


